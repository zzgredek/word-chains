package com.wordchains;

import java.util.Arrays;
import org.junit.Test;

import com.wordchains.algorithm.WordChainFinder;
import com.wordchains.wordlist.WordListSupplier;

import static org.junit.Assert.*;

public class WordChainTest {
	
	WordListSupplier wordListSupplier = new WordListSupplier();

	@Test
	public void testFindChainForThreeLetterWord() {
		WordChainFinder wordChain = new WordChainFinder(wordListSupplier.getWordListByLength(3));
		assertEquals(Arrays.asList("cat", "dat", "dot", "dog"), wordChain.findChain("cat", "dog"));
	}
	
	@Test
	public void testFindChainForThreeLetterWordBackwards() {
		WordChainFinder wordChain = new WordChainFinder(wordListSupplier.getWordListByLength(3));
		assertEquals(Arrays.asList("dog", "cog", "cot", "cat"), wordChain.findChain("dog", "cat"));
	}
	
	@Test
	public void testFindChainForThreeLetterWordRepeatability() {
		WordChainFinder wordChain = new WordChainFinder(wordListSupplier.getWordListByLength(3));
		assertEquals(Arrays.asList("cat", "dat", "dot", "dog"), wordChain.findChain("cat", "dog"));
		assertEquals(Arrays.asList("cat", "dat", "dot", "dog"), wordChain.findChain("cat", "dog"));
	}
	
	@Test
	public void testFindChainForFourLetterWord() {
		WordChainFinder wordChain = new WordChainFinder(wordListSupplier.getWordListByLength(4));
		assertEquals(Arrays.asList("ruby", "rube", "robe", "rode", "code"), wordChain.findChain("ruby", "code"));
	}
	
	@Test
	public void testErrorConditions() {
		WordChainFinder wordChain = new WordChainFinder(wordListSupplier.getWordListByLength(3));
		assertTrue(wordChain.findChain("cat", "asd").size() == 0);
	}

}
