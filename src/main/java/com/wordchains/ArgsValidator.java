package com.wordchains;

import org.apache.log4j.Logger;

public class ArgsValidator {
	
	private static Logger logger = Logger.getLogger(ArgsValidator.class);

	public static void validate(String[] args) {
		if(args.length != 2) {
			logger.error("Usage: [startWord] [endWord]");
			throw new IllegalArgumentException("Too few arguments");
		}
		
		if(args[0].length() != args[1].length()) {
			logger.error("Both words have to have same length");
			throw new IllegalArgumentException();
		}
	}
}	
