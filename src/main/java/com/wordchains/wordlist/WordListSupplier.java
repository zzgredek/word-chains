package com.wordchains.wordlist;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

public class WordListSupplier implements Supplier<List<String>> {
	
	private Logger logger = Logger.getLogger(WordListSupplier.class);
	private List<String> wordList = new ArrayList<>();

	public WordListSupplier() {
		readWordListFile("wordlist.txt");
	}

	private void readWordListFile(String fileName) {
		try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
			wordList = br.lines().collect(Collectors.toList());
		} catch (IOException e) {
			logger.error("Cannot load wordlist");
		}
	}

	@Override
	public List<String> get() {
		return new ArrayList<>(wordList);
	}

	public List<String> getWordListByLength(int length) {
		return wordList.stream().filter(word -> word.length() == length).collect(Collectors.toList());
	}
}
