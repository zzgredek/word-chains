package com.wordchains.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;

public class WordChainFinder {

	private List<String> wordList;
	private List<String> exclusionWordList;

	public WordChainFinder(List<String> wordList) {
		this.wordList = wordList;
	}

	public List<String> findChain(String startWord, String endWord) {
		exclusionWordList = new ArrayList<>(wordList);
		Map<String, String> processedWordsMap =  new HashMap<>();
		Queue<String> queue = new LinkedList<>();
		queue.add(startWord);
		processedWordsMap.put(startWord, null);
		String processedWord = startWord;
		
		while(!processedWord.equals(endWord) && !queue.isEmpty()) {
			processedWord = queue.remove();
			
			List<String> simillarWordList = getSimilarWordsList(processedWord);
			for (String simillarWord : simillarWordList) {
				if(!processedWordsMap.containsKey(simillarWord)) {
					queue.add(simillarWord);
					processedWordsMap.put(simillarWord, processedWord);
					exclusionWordList.remove(simillarWord);
				}					
			}
		}
		
		if(!processedWordsMap.containsKey(endWord)) {
			return Collections.emptyList();
		}

		return getWordChainFromEndWord(processedWordsMap, endWord);
	}

	private List<String> getWordChainFromEndWord(Map<String, String> processedWordsMap, String endWord) {
		List<String> wordChain = new ArrayList<>();
		Optional<String> word = Optional.ofNullable(endWord);

		while (word.isPresent()) {
			wordChain.add(word.get());
			word = Optional.ofNullable(processedWordsMap.get(word.get()));
		}

		Collections.reverse(wordChain);
		
		return wordChain;
	}
	
	private List<String> getSimilarWordsList(String parentWord) {
		List<String> similarWordsList = new ArrayList<>();

		for (int i = 0; i < parentWord.length(); i++) {
			for (String word : exclusionWordList) {
				if (word.matches(getRegex(parentWord, i)))
					similarWordsList.add(word);
			}
		}

		return similarWordsList;
	}

	private String getRegex(String str, int index) {
		return str.substring(0, index) + "." + str.substring(index + 1);
	}

}
