package com.wordchains;

import java.util.List;

import com.wordchains.algorithm.WordChainFinder;
import com.wordchains.wordlist.WordListSupplier;

public class Application {

	
	public static void main(String[] args) {
		ArgsValidator.validate(args);
		String startWord = args[0];
		String endWord = args[1];
		
		WordListSupplier wordList = new WordListSupplier();
		
		WordChainFinder wordChain = new WordChainFinder(wordList.getWordListByLength(startWord.length()));
		
		List<String> wordChainList = wordChain.findChain(startWord, endWord);
		System.out.println(wordChainList);
		
	}
}
